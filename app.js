'use strict'

const config = require('./config.json');
const express = require('express');
const HTTPStatus = require('http-status-codes');
const logger = require('morgan');
const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');

const app = express();

// Load routes
const routes = {
    authors: require('./routes/authors'),
    books: require('./routes/books')
};

// Connect to database

mongoose.connect(config.mongoose.connectionString, config.mongoose.options);
const db = mongoose.connection;

db.on('error', (err) => {
    console.log(`database error`, err);
});

db.on('connecting', () => {
    console.log(`connecting to the database`);
});

db.once('open', () => {
    console.log(`database connection established`);
});

mongoose.plugin(mongooseUniqueValidator);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// setup routes
app.use('/books', routes.books);
app.use('/authors', routes.authors);

// error handlers
app.use((req, res, next) => {
    res.sendStatus(HTTPStatus.NOT_FOUND);
    next();
});

process.on('unhandledRejection', (reason, p) => {
    console.log(`unhandled rejection`, p, reason);
});

process.on('uncaughtException', (err) => {
    console.log(`uncaught exception`, err);
});

app.listen(config.app.port, () => {
    console.log(`app listening on port ${config.app.port}`);
});
