'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Author schema

const Author = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
});

module.exports = mongoose.model('Author', Author);
