'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Book schemas

const Book = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'Author',
        required: true
    }
});

module.exports = mongoose.model('Book', Book);
