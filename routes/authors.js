'use strict'

const express = require('express');
const router = express.Router();
const HTTPStatus = require('http-status-codes');
const authorModel = require('../models/Author');

// create author
router.post('/', (req, res) => {
    let author = new authorModel(req.body);

    author.save()
      .then(doc => {
         res.send(doc);
      })
      .catch(err => {
        res.status(HTTPStatus.BAD_REQUEST);
        res.send(err.message);
      });
});

// get all authors

router.get('/', (req, res) => {
    authorModel.find()
      .then(docs => {
        res.send(docs);
      })
      .catch(err => {
        res.status(HTTPStatus.BAD_REQUEST);
        res.send(err.message);
      });
});



// get books by author
router.get('/:id', (req, res) => {
    bookModel.find({author: req.params.id})
    .then(docs => {
        res.send(docs || []);
    })
    .catch(err => {
      res.status(HTTPStatus.BAD_REQUEST);
      res.send(err.message);
    });
});

module.exports = router;
