'use strict'

const express = require('express');
const router = express.Router();
const HTTPStatus = require('http-status-codes');
const bookModel = require('../models/Book');

// get all books
router.get('/', (req, res) => {
    bookModel.find()
      .then(docs => {
        res.send(docs);
      })
      .catch(err => {
        res.status(HTTPStatus.BAD_REQUEST);
        res.send(err.message);
      });
});

// create book
router.post('/', (req, res) => {
    let book = new bookModel(req.body);

    book.save()
      .then(doc => {
         res.send(doc);
      })
      .catch(err => {
        res.status(HTTPStatus.BAD_REQUEST);
        res.send(err.message);
      });
});

module.exports = router;
